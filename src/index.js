import React, { useState } from "react";
import ReactTable from "react-table";
import ReactDOM from "react-dom";
import { contacts } from "./mockCustomerContact";
import "react-table/react-table.css";
import Modal from "./Modal";
import { Form, Button } from "react-bootstrap";

function ContactTable() {
  const data = contacts;

  const [isModalOpen, toggleModal] = useState(false);
  const [detail, setContactDetails] = useState({ contact: "", address: "" });

  return (
    <div className="ContactTable">
      <Modal isOpen={isModalOpen} toggle={toggleModal} data={detail}>
        <Form>
          <Form.Group controlId="formBasicPhone">
            <Form.Label>Phone</Form.Label>
            <Form.Control type="text" value={detail.contact.phone} />
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="text" value={detail.contact.email} />
          </Form.Group>
          <Form.Group controlId="formBasicCity">
            <Form.Label>City</Form.Label>
            <Form.Control type="text" value={detail.address.city} />
          </Form.Group>
        </Form>
      </Modal>
      <Button variant="primary" type="submit">
        Create Contact
      </Button>

      <ReactTable
        className="-striped -highlight h-full w-full sm:rounded-16 overflow-hidden"
        getTrProps={(state, rowInfo, column) => {
          return {
            className: "cursor-pointer",
            onClick: (e, handleOriginal) => {
              if (rowInfo) {
                setContactDetails(rowInfo.original);
                if (detail != null) {
                  toggleModal(!isModalOpen);
                }
              }
            }
          };
        }}
        data={data}
        columns={[
          {
            accessor: "id",
            Cell: row => (
              <div
                className="mr-8"
                alt={row.original.name}
                src={row.original.id}
              />
            ),
            className: "justify-center",
            width: 64,
            sortable: false
          },
          {
            Header: "Id",
            accessor: "id",
            width: 100
          },
          {
            Header: "Customer Id",
            accessor: "customer_id",
            className: "font-bold"
          },
          {
            Header: "Email",
            accessor: "contact.email"
          },
          {
            Header: "Phone",
            accessor: "contact.phone"
          },
          {
            Header: "Street",
            accessor: "address.street"
          },
          {
            Header: "City",
            accessor: "address.city"
          }
        ]}
        defaultPageSize={10}
        noDataText="No Contacts found"
      />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<ContactTable />, rootElement);
