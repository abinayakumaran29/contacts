export const contacts = [
  {
    id: 1,
    contact: {
      email: "adam@live.com",
      phone: "+9083467827382"
    },
    customer_id: 4,
    address: {
      city: "California",
      state: "CA",
      street: "Bill Street"
    },
    company_id: 9
  },
  {
    id: 2,
    contact: {
      email: "martin@live.com",
      phone: "+877827382"
    },
    customer_id: 5,
    address: {
      city: "San Diego",
      state: "SD",
      street: "Lerf Street"
    },
    company_id: 10
  },
  {
    id: 3,
    contact: {
      email: "james@live.com",
      phone: "+673467827382"
    },
    customer_id: 6,
    address: {
      city: "San fransisco",
      state: "CA",
      street: "Park Street"
    },
    company_id: 11
  },
  {
    id: 4,
    contact: {
      email: "asha@live.com",
      phone: "+4583467827382"
    },
    customer_id: 7,
    address: {
      city: "Mexico",
      state: "MX",
      street: "Huy Street"
    },
    company_id: 12
  }
];
